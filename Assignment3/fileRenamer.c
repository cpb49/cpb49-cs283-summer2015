#include <string.h>
#include <stdio.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <ftw.h>

#include "file_functions.h"


int main(int argc, char *argv[]){

	if (argc < 4) {
		printf(" Usage: ./function.out path find replace");
	}
	/* make the arguments accessible to the library */	
	arguments = argv;
	numArguments = argc;
	/* returns the result of ftw */

	return ftw( argv[1], replace, 100);

}
