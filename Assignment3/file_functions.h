#include <string.h>
#include <regex.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <ftw.h>

extern char** arguments;
extern int numArguments;

char* getFile(const char* filename, long* numbytes);

int find_replace(const char* fpath);

int replace(const char *fpath, const struct stat *sb, int typeflag);

int file_replace(const char *fpath, const struct stat *sb, int typeflag);


