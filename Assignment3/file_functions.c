#include <string.h>
#include <stdio.h>
#include <regex.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdlib.h>
#include <ftw.h>

#define MAX_ERROR_MSG 0x1000

char** arguments;
int numArguments;

/*
 * Function:  getFile
 * --------------------
 *  Slurps a file into memory as a string.
 *
 *  filename: valid filename
 *  numbytes: how big the file is in bytes, return value as well!
 *
 *  returns: file contents and file length in bytes
 */

char* getFile(const char* filename, long* numbytes){
		
	/* declare a file pointer */
	FILE    *infile;
	char    *buffer;
 
	/* open an existing file for reading */
	infile = fopen(filename, "r");
 
	/* quit if the file does not exist */
	if(infile == NULL) {
		printf("file does not exist\n");
		return NULL;
 	}
	/* Get the number of bytes */
	fseek(infile, 0L, SEEK_END);
	*numbytes = ftell(infile);
 
	/* reset the file position indicator to the beginning of the file */
	fseek(infile, 0L, SEEK_SET);	
 
	/* grab sufficient memory for the buffer to hold the text */
	buffer = (char*)malloc((*numbytes)*sizeof(char)+1);	
 
	/* memory error */
	if(buffer == NULL){
		printf("memory error\n");
		return NULL;
 	}
	/* copy all the text into the buffer */
	size_t result = fread(buffer, sizeof(char), *numbytes, infile);
	buffer[*numbytes] = '\0';
	if (result != *numbytes) perror("fread failed\n");
	fclose(infile);
	return buffer;
}

/*
 * Function:  find_replace
 * --------------------
 *  Performs find/replace on a filename.
 *
 *  fpath: valid file path
 *  
 *
 *  returns: int error code interpreted by ftw()
 */

int find_replace(const char* fpath){
	if (fpath == NULL) return -1;
 
	if (numArguments == 5){
		if (arguments[4] == "") {
			;
		} else {
			char* result = strstr(fpath, arguments[4]);
			if (result == NULL){
			 return 0;
			}
		}
	
	}
	
	char* curr = malloc(sizeof(char)*(strlen(fpath)+1));
	char* currStart = curr;
	if (curr == NULL) {
		return -1;
	}

	strncpy(curr, fpath, strlen(fpath)+1);
	
	char* temp = curr;
	char* newName = (char*) malloc(sizeof(char)*strlen(fpath)*4); //this should be good enough
	if (newName == NULL) {
		free(currStart);
		return -1;
	}

	char* newCurr = newName;
	printf("finder\n");
	temp = strstr(strrchr(curr, '/'), arguments[2]);
	if (temp == NULL){
		free(currStart);
		free(newName);
		return 0; // we're done!
	}
	while (temp != NULL){ 
		while (curr != temp){
			*newCurr = *curr; //copy over one by one
			curr++;
			newCurr++;
		}
		//strncat(newName, arguments[2], strlen(arguments[2]));
		curr = arguments[3];
		while (*curr != '\0'){
			*newCurr = *curr; //copy over one by one
			curr++;
			newCurr++;
		}
		curr = temp;
		curr = curr + strlen(arguments[2]);
		temp = strstr(curr, arguments[2]);
	}
	//if we've gotten here there arbe no more matches but there may be dangly string bits
	while (*curr != '\0'){
		*newCurr = *curr;
		curr++;
		newCurr++;
	}
	*newCurr = '\0';
	free(currStart);
	

	printf("Moving %s to %s\n",fpath,newName);

	int ret = rename(fpath, newName);
	free(newName);
	return ret;
	
}

/*
 * Function:  replace
 * --------------------
 *  FTW wrapper for find_replace
 *
 *  fpath: valid file path
 * 	sb, typeflag: given by FTW
 *  
 *
 *  returns: int error code interpreted by ftw()
 */

int replace(const char *fpath, const struct stat *sb, int typeflag){
	if ((typeflag == FTW_D) || !strcmp(fpath, "..") || !strcmp(fpath, ".")) return 0;
	if (typeflag == FTW_F){		
		int error = find_replace(fpath);

		if (error != 0) perror("File error");
		
	}
	return 0;
}

/*
 * Function:  file_replace
 * --------------------
 *  FTW wrapper, performs find/replace on file contents
 *
 *  fpath: valid file path
 * 	sb, typeflag: given by FTW
 *  
 *
 *  returns: int error code interpreted by ftw()
 */

int file_replace(const char *fpath, const struct stat *sb, int typeflag){
	if (numArguments == 5){
		if (arguments[4] == "") {
			;
		} else {
			char* result = strstr(fpath, arguments[4]);
			if (result == NULL){
			 return 0;
			}
		}
	
	}
	
	if (fpath == NULL) return -1;
	if ((typeflag == FTW_D) || !strcmp(fpath, "..") || !strcmp(fpath, ".")) return 0;
	
	long* numbytes = malloc(sizeof(long));
	if (numbytes == NULL) return -1;

	char* curr = getFile(fpath, numbytes);

	if (curr == NULL){
		 free(numbytes);			
		 return -1;
	}
	char* temp = curr;
	char* newName = malloc(sizeof(char)*(*numbytes)*4); //this should be good enough
	free(numbytes);
	if (newName == NULL){
		 free(curr);
		 free(numbytes);
		 return 13;
	}
	
	temp = strstr(curr, arguments[2]);
	if (temp == NULL) {
		free(curr);
		free(newName);
		return 0; // we're done!
	}
	printf("Replacing pattern in %s\n", fpath);
	char* newCurr = newName;
	while (temp != NULL){ 
		while (curr != temp){
			*newCurr = *curr; //copy over one by one
			curr++;
			newCurr++;
		}
		//strncat(newName, arguments[2], strlen(arguments[2]));
		curr = arguments[3];
		while (*curr != '\0'){
			*newCurr = *curr; //copy over one by one
			curr++;
			newCurr++;
		}
		curr = temp;
		curr = curr + strlen(arguments[2]);
		temp = strstr(curr, arguments[2]);
	}
	//if we've gotten here there are no more matches but there may be dangly string bits
	while (*curr != '\0'){
		*newCurr = *curr;
		curr++;
		newCurr++;
	}
	free(temp);
	*newCurr = '\0';
	
	printf("Writing to file %s\n", fpath);
	FILE* outfile = fopen(fpath, "w");

	size_t writer = fwrite(newName, sizeof(char), strlen(newName), outfile);
	

	fclose(outfile);


	if (writer != strlen(newName)){

		printf("ERROR: Halfwritten file: %s", fpath);
		return -1;
	}
	free(newName);
	return 0;
}




