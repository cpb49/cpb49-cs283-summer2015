#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include "anagram.h"

#define __HASH_SPLIT__ 400
/* there are ~400000 words in the linux dictionary. Adding 400
   slots to the hash table gives me a search space of about 1000
   words on average. Heavy to create, fast to search is what 
   I'm going for. */

int main(int argc, char *argv[] ){


	if (argc > 1){

	


	FILE* dict = fopen("/usr/share/dict/words", "r"); //open the dictionary for read-only access
    if(dict == NULL) {
        return 1;
    }
	clock_t start_t, end_t;
    // Read each line of the file, and print it to screen
    char* word = malloc(sizeof(char)*128);
	hash_table* my_hash_table = create_hash_table(__HASH_SPLIT__);
    start_t = clock();	
    while(fgets(word, sizeof(char)*128, dict) != NULL) {
//	for(int i = 0; i < __HASH_SPLIT__; i++){
//		fgets(word, 128*sizeof(char), dict);
		word = strtok(word, "\n");
//		printf("%s\n", word);
        add_string(my_hash_table, word);
    }

    end_t = clock();
    printf("Finished building directory in %f seconds \n", (double)(end_t - start_t)/ CLOCKS_PER_SEC);


	free(word);
	char* foo = malloc(sizeof(char)*7);
	start_t = clock();
	//strncpy(foo, "listen", 7);
	list* result = get_anagrams(my_hash_table, argv[1]);
	end_t = clock();
	printf("Finished searching directory in %f seconds \n", (double)(end_t - start_t)/ CLOCKS_PER_SEC);
	list* temp;
	while(result != NULL){
		//printf("results!");
		printf("%s\n", result->str);
		temp = result;
		result = result->next;
		free(temp->str);
		free(temp);
	}
	
	fclose(dict);
	free(foo);
	free_table(my_hash_table);

	}
	return 0;

}

