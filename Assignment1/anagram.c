#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "anagram.h"

/*
 * Function:  create_hash_table 
 * --------------------
 * Creates a new hash table for the user.
 *
 *  size: number of items in the hash table
 *
 *  returns: a pointer to the new hashtable
 */
hash_table* create_hash_table(int size)
{
    hash_table* new_table;
    
    if (size<1) return NULL;

    if ((new_table = malloc(sizeof(hash_table))) == NULL) {
        return NULL;
    }
    
	/* Attempt to allocate memory for the table itself */
    if ((new_table->table = malloc(sizeof(list *) * size)) == NULL) {
        return NULL;
    }

    /* Initialize the elements of the table */
    for(int i=0; i<size; i++) {
		new_table->table[i] = NULL;
	}

    /* Set the table's size */
    new_table->size = size;

    return new_table;
}

/*
 * Function:  hash
 * --------------------
 * Calculates a hash for a word.
 *
 *  hashtable: the hashtable this hash will be placed in
 *  str: the string to hash
 *
 *  returns: the hash as an unsigned int
 */
unsigned int hash(hash_table* hashtable, char* str)
{
	//if we have negative indexes we'll be in deep trouble
    unsigned int hashval = 0;
	
    for(; *str != '\0'; str++) {

		if (*str < 97) {
			hashval = hashval + (*str)+32-96;
			//*str = *str + 32;
			// normalize capital letters
		} else {
		//use the A=1, B=2... scheme to hash
			hashval = hashval + (*str - 96) ;
		}

	}

    return hashval % hashtable->size;
}

/*
 * Function:  lookup 
 * --------------------
 * Finds a string in a hash table.
 *
 *  hashtable: the hashtable to seach
 *  str: the string to find
 *
 *  returns: a pointer to the node containing the target string
 */
list* lookup(hash_table* hashtable, char* str)
{
    list* list_ele;
    unsigned int hashval = hash(hashtable, str);

    for(list_ele = hashtable->table[hashval]; list_ele != NULL; list_ele = list_ele->next) {
		//printf("%s\n",list_ele->str);
        if (strcmp(str, list_ele->str) == 0) {
			return list_ele;
		}
    }
    return NULL;
}


/*
 * Function:  add_string 
 * --------------------
 * Adds a string to a hashtable.
 *
 *  hashtable: the hashtable to insert into
 *  str: the string to add
 *
 *  returns: an int code. 1 = memory fail; 2 = string exists; 0 = success.
 */
int add_string(hash_table *hashtable, char* str)
{
    list* new_list;
    list* current_list;
    unsigned int hashval = hash(hashtable, str);

    if ((new_list = malloc(sizeof(list))) == NULL) {
		return 1;
	}
    /* Does item already exist? Unlikely, but possible */
    current_list = lookup(hashtable, str);
    if (current_list != NULL) {
		//30 minutes later I find this memory leak. goddamn it!
		free(new_list);
		return 2;
	}
	new_list->str = (char*) malloc(128);
	//TODO magic number
	//printf("%s\n",str);
    strncpy(new_list->str,str,128);
	//new_list->str = strdup(str);
    new_list->next = (*hashtable).table[hashval];
    hashtable->table[hashval] = new_list;

    return 0;
}

/*
 * Function:  free_table
 * --------------------
 * Destroys a hashtable structure
 *
 *  hashtable: the hashtable to destroy
 *  
 *  returns: void
 */
void free_table(hash_table* hashtable)
{
    int i;
    list* list_ele, *temp;

    if (hashtable==NULL) return;

    /* Free the memory for every item in the table, including the 
     * strings themselves.
     */
    for(i=0; i<hashtable->size; i++) {
        list_ele = hashtable->table[i];
        while(list_ele!=NULL) {
            temp = list_ele;
            list_ele = list_ele->next;
            free(temp->str);
            free(temp);
        }
		
    }

    /* Free the table itself */
    free(hashtable->table);
    free(hashtable);
}

/*
 * Function:  get_anagrams 
 * --------------------
 * Finds the anagrams of a word.
 *
 *  hashtable: the hashtable to search
 *  anagram: the word to find anagrams of
 *
 *  returns: a list* to a linked list of matches
 */
list* get_anagrams(hash_table* hashtable, char* anagram){
	unsigned int hashval = hash(hashtable, anagram);
	list* list_ele;
	int* counter = malloc(sizeof(int)*127);
	//bucket sort -- 127 buckets for each ascii character
	list* HEAD = NULL;
	list* current_node;
	char* currentAnagram;
	char* currentListEle;
	

	for(list_ele = hashtable->table[hashval]; list_ele != NULL; list_ele = list_ele->next) {
		//temporary pointers to the strings
		currentAnagram = anagram;
		currentListEle = (list_ele->str);
		
		while ((*currentAnagram != '\0') && (*currentListEle != '\0')){
			for(int i = 0; i < 127; i++){
				counter[i] = 0; //initialize with zeros just to be sure 
			}	
			//if the strings are anagrams, these operations should cancel out
			printf("%c %c\n", *currentAnagram, *currentListEle);
			counter[(int) *currentAnagram] = counter[(int) *currentAnagram] + 1;
			counter[(int) *currentListEle] = counter[(int) *currentListEle] - 1;
			currentAnagram++;
			currentListEle++;
		}
		if(*currentAnagram != *currentListEle){
			//if both pointers are not at the terminator, then we have failed
			//printf("%s\n", "size is wrong");
			continue;
		}
		int sum = 0;
		printf("%s\n", list_ele->str);
		for (int i = 0; i < 127; ++i) {
			if(counter[i] != 0) {
				printf("wrong char: %c\n", (char) i);
				sum = 1;
				break;
			}
			//printf("%d", counter[i]);
  			//sum |= counter[i]; //bitwise or operation
		}
		if (sum != 0) {
			//printf("%s\n", "sum is wrong");
  			continue; //something was not balanced out correctly
		}
		
		//make a node for our match
		//printf("%s\n", list_ele->str);
		current_node = malloc(sizeof(list));
		current_node->next = HEAD;
		HEAD = current_node;
		current_node->str = (char*) malloc(128);
		strncpy(current_node->str,list_ele->str,128);
		
	}
	free(counter);
	return HEAD;
}





