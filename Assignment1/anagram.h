#define __HASH_SPLIT__ 400
/* there are ~400000 words in the linux dictionary. Adding 400
   slots to the hash table gives me a search space of about 1000
   words on average. Heavy to create, fast to search is what 
   I'm going for. */
#define __LONGEST_WORD__ 46
/* the longest word in the linux dictionary + 1 for the null term. */
typedef struct _list {
    char* str;
    struct _list* next;
} list;

typedef struct _hash_table {
    int size;       /* the size of the table */
    list** table; /* the table elements */
} hash_table;


/*
 * Function:  create_hash_table 
 * --------------------
 * Creates a new hash table for the user.
 *
 *  size: number of items in the hash table
 *
 *  returns: a pointer to the new hashtable
 */
hash_table* create_hash_table(int size);

/*
 * Function:  hash
 * --------------------
 * Calculates a hash for a word.
 *
 *  hashtable: the hashtable this hash will be placed in
 *  str: the string to hash
 *
 *  returns: the hash as an unsigned int
 */
unsigned int hash(hash_table* hashtable, char* str);


/*
 * Function:  lookup 
 * --------------------
 * Finds a string in a hash table.
 *
 *  hashtable: the hashtable to seach
 *  str: the string to find
 *
 *  returns: a pointer to the node containing the target string
 */
list* lookup(hash_table* hashtable, char* str);

/*
 * Function:  add_string 
 * --------------------
 * Adds a string to a hashtable.
 *
 *  hashtable: the hashtable to insert into
 *  str: the string to add
 *
 *  returns: an int code. 1 = memory fail; 2 = string exists; 0 = success.
 */
int add_string(hash_table* hashtable, char* str);

/*
 * Function:  free_table
 * --------------------
 * Destroys a hashtable structure
 *
 *  hashtable: the hashtable to destroy
 *  
 *  returns: void
 */
void free_table(hash_table* hashtable);

/*
 * Function:  get_anagrams 
 * --------------------
 * Finds the anagrams of a word.
 *
 *  hashtable: the hashtable to search
 *  anagram: the word to find anagrams of
 *
 *  returns: a list* to a linked list of matches
 */
list* get_anagrams(hash_table* hashtable, char* anagram);


