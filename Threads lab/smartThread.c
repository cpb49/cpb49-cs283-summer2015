#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define NUM_THREADS 100
#define SEAT_MOVEMENTS 1000

pthread_mutex_t securityGuard = PTHREAD_MUTEX_INITIALIZER;

void *Person(void* counter){
	int* c = (int*) counter;
	for(int i = 0; i < SEAT_MOVEMENTS; i++){
		pthread_mutex_lock(&securityGuard);
		(*c)++;
		pthread_mutex_unlock(&securityGuard);
	}
	return NULL;
}


int main(int argc, char* argv[]){

	clock_t begin, end;
	double time_spent;
	begin = clock();

	void* status;
	pthread_attr_t attr;
	
	pthread_mutex_init(&securityGuard, NULL);
	pthread_t threads[NUM_THREADS];
	void* counter = calloc(sizeof(int), 1);
	int rc;
	long i;
	for(i=0; i<NUM_THREADS;i++){
		rc = pthread_create(&threads[i], NULL, Person, counter);
		pthread_attr_destroy(&attr);
		if (rc){
			printf("Thread error: %d\n", rc);
			exit(-1);
		}
	}
	for(i=0; i<NUM_THREADS;i++) {
		pthread_join(threads[i], &status);
	}
	end = clock();
	pthread_mutex_destroy(&securityGuard);
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("counter: %d\n time: %f\n", *((int*)counter), time_spent);
	
}
