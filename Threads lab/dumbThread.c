#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#define NUM_THREADS 100
#define SEAT_MOVEMENTS 1000

void *Person(void* counter){
	int* c = (int*) counter;
	for(int i = 0; i < SEAT_MOVEMENTS; i++){
		(*c)++;
	}
	return NULL;
}

int main(int argc, char* argv[]){

	clock_t begin, end;
	double time_spent;
	begin = clock();

	pthread_t threads[NUM_THREADS];
	void* counter = calloc(sizeof(int), 1);
	int rc;
	long i;
	for(i=0; i<NUM_THREADS;i++){
		rc = pthread_create(&threads[i], NULL, Person, counter);
		if (rc){
			printf("Thread error: %d\n", rc);
			exit(-1);
		}
	}
	for(i=0; i<NUM_THREADS;i++) {
		pthread_join(threads[i], NULL);
	}
	end = clock();
	time_spent = (double)(end - begin) / CLOCKS_PER_SEC;
	printf("counter: %d\n time: %f\n", *((int*)counter), time_spent);
	
}
