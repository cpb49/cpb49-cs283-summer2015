import client
import argparse

def main():
	parser = argparse.ArgumentParser(description='Become a skiddie.')
	parser.add_argument('--public', metavar='publicKey', nargs=1, type=int,
                   help='the public key', dest="e", required=True)
	parser.add_argument('--constant', metavar='constant', type=int, nargs=1, required=True,
                   help='the constant you possess', dest="n")

	args = parser.parse_args()

	public = args.e[0]
	constant = args.n[0]
	decrypted = ''
	data = "fosdfsgfsd"

	encrypted = client.encrypt(data, public, constant)
	d = 2
	while data != decrypted:
		decrypted = client.decrypt(encrypted, d, constant)
		d += 1

	print("The private key is " + str(d))

	while True:
		target = input("Char to Decrypt: ")
		print(client.decrypt(bytes(target, 'ascii'), d, constant))

if __name__ == "__main__":
	main()

