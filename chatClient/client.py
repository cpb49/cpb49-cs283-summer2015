#!/usr/bin/python3
import socket
import sys
import argparse
import _thread
import time
import fractions

semaphore = True
BYTE_PACKAGE = 4
SIZE_INT = 4
prime1 = 1
prime2 = 1

def main():

	PORT_RANGE = list(range(1,65535))

	parser = argparse.ArgumentParser(description='Chat with a friend.')
	parser.add_argument('--host', metavar='DestinationHost', nargs=1,
                   help='the host to connect to in ipv4 format', dest="host", required=True)
	parser.add_argument('--port', metavar='DestinationPort', type=int, nargs=1, required=True,
                   help='the listening port on the host', dest="port")
	parser.add_argument('--prime1', metavar='Prime', type=int, nargs=1, required=True,
                   help='the first prime', dest="x")
	parser.add_argument('--prime2', metavar='Prime', type=int, nargs=1, required=True,
                   help='the second prime', dest="y")
	

	args = parser.parse_args()

	port = args.port[0]
	host = args.host[0]
	global prime2
	global prime1
	prime1 = args.x[0]
	prime2 = args.y[0]

	if not isPrime(prime1) or not isPrime(prime2):
		print("Invalid primes")
		sys.exit(1)

	if port not in PORT_RANGE:
		print("Invalid Port. Port range: 1-65535")
		sys.exit(1)
	
	
	#check if we are the server or the client
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	s.settimeout(5)
	result = s.connect_ex((host, port))

	if result == 0:
		client(s)
		pass
	else:
		#we are the server
		s.close()
		server(port)
	while semaphore:
		pass
	time.sleep(1)

def server(port):
	print("Waiting for connections")
	#s.bind((socket.gethostbyname(socket.gethostname()), port))
	s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
	#s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
	HOST = ''
	s.settimeout(None)
	s.bind((HOST, port))
	s.listen(1)
	conn, addr = s.accept()
	print("Accepted Connection from " + str(addr))
	### BLOCKING CALL ###
	myKey, myConstant = sendKey(conn)
	theirKey = int.from_bytes(conn.recv(4096), 'big')
	#print("Key" + str(theirKey))
	theirConstant = int.from_bytes(conn.recv(4096), 'big')
	#print("Constant" + str(theirConstant))
	listener = _thread.start_new_thread(listen, (conn,semaphore, myKey, myConstant))
	speaker = _thread.start_new_thread(send, (conn,semaphore, theirKey, theirConstant))

def byteChunking(byteString):
	intList = []
	for i in range(0, len(byteString), SIZE_INT):
		intList.append(int.from_bytes(byteString[i : i+4], 'big'))
	#print(intList)
	return intList

def isPrime(n):
    for i in range(2,int(n**0.5)+1):
        if n%i==0:
            return False
    return True
		
def sendKey(s):
	s.settimeout(None)
	constant, public, private = genKey()
	s.sendall(int(public).to_bytes(SIZE_INT, 'big'))
	time.sleep(1)
	s.sendall(int(constant).to_bytes(SIZE_INT, 'big'))
	return private, constant	

def listen(s, semaphore, myKey, myConstant):
	while semaphore:
		s.settimeout(None)
		data = s.recv(1024)
		if not data: break
		message = decrypt(data, myKey, myConstant)
				
		print('Friend: ' + message + '\n')
	#s.sendall(0)
	#s.close()

def send(s, semaphore, theirKey, theirConstant):
	while semaphore:
		message = input('-->You: ')
		if message == "END CHAT":
			endChat()
			break;
		cryptoMessage = encrypt(message, theirKey, theirConstant)
		s.sendall(cryptoMessage)
	s.sendall(bytes(chr(0), "UTF_8"))
	s.close()

def endChat():
	global semaphore
	semaphore = False
	print("\n-->Chat Over.")
	sys.exit(0)

def client(s):
	s.settimeout(None)
	theirKey = int.from_bytes(s.recv(4096), 'big')
	#print("Server Key: " + str(theirKey))
	theirConstant = int.from_bytes(s.recv(4096), 'big')
	myKey, myConstant = sendKey(s)
	listener = _thread.start_new_thread(listen, (s,semaphore, myKey, myConstant))
	speaker = _thread.start_new_thread(send, (s,semaphore, theirKey, theirConstant))

def encrypt(data, theirKey, theirConstant):
	ret = []
	for char in data:
		c = cryptoMath(ord(char), theirKey, theirConstant)
		#print(c)
		ret.append(int(c).to_bytes(SIZE_INT, 'big'))
	#print('ret', ret)
	return b''.join(ret)
	

def decrypt(data, myKey, myConstant):
	intData = byteChunking(data)
	result = ''.join(chr(cryptoMath(i, myKey, myConstant)) for i in intData)
	return result

def genKey():
	x = prime1
	y=prime2
	n = x*y
	totient = (x-1)*(y-1)
	e = 2
	while fractions.gcd(e,totient) != 1:
		e += 1
	# e is now public key
	if e > totient:
		print ("Fucker.")
	d = (1 + 1234*totient)
	while (d % e != 0):
		d += 1
	decryptor = d//e
	#print("Key Generated", n, e, decryptor)
	return n, e, decryptor
		
def cryptoMath(base, exponent, modulus):
	c = 1
	for i in range(exponent):
		c = (base*c) % modulus
	return int(c)

if __name__ == "__main__":
	try:
		main()
	except KeyboardInterrupt:
		endChat()
